import express from 'express';
import { User, UserDto } from '../dataModel/user';
import UserService from '../services/userService';
import urlGenerator from '../utilities/urlGenerator';
import { Routes } from './routesDefinition';
import { PaginatedResponse } from '../http/httpTypes';
import EventService from '../services/eventService';

export const userRouter = express.Router();

userRouter.get('/', async (request, response) => {
    try {
        // could return error if page and limit cannot be converted to number
        const page = Number(request.query.page) || 1;
        const limit = Number(request.query.limit) || 10;
        const paginationResults = await UserService.list(page, limit);

        const responseObject : PaginatedResponse<User> = {
            data: paginationResults.results.map(result => ({
                result,
                selfLink: `${urlGenerator.formUrlFromRoute(Routes.User)}/${result._id}`
            })),
            count: paginationResults.count
        };

        const nextUrl = urlGenerator.formNextPaginationUrl(Routes.User, page, limit, paginationResults.count);
        if(!!nextUrl) {
            responseObject.nextLink = nextUrl;
        }

        const previousUrl = urlGenerator.formPreviousPaginationUrl(Routes.User, page, limit);
        if (!!previousUrl) {
            responseObject.previousLink = previousUrl;
        }

        response.status(200).json(responseObject);
    } catch (error) {
        response.status(500).json({
            message: `Could not retrieve users`,
            error
        });
    }
});

userRouter.get('/:id', async (request, response) => {
    const id = request.params.id;
    try {
        const user : User = await UserService.findById(id);
        response.status(200).json({
            message: `Found user with id: ${id}`,
            selfLink: `${urlGenerator.formUrlFromRoute(Routes.User)}/${user._id}`,
            data: user
        });
    } catch(error) {
        response.status(404).json({
            message: `Could not find user with id: ${id}`,
            error
        });
    }
});

userRouter.get('/find/:email', async (request, response) => {
    const email = request.params.email;
    try {
        const user : User = await UserService.findByEmail(email);
        response.status(200).json({
            message: `Found user with email: ${email}`,
            selfLink: `${urlGenerator.formUrlFromRoute(Routes.User)}/${user._id}`,
            data: user
        });
    } catch (error) {
        response.status(404).json({
            message: `Could not find user with email: ${email}`,
            error
        });
    }
});

userRouter.post('/', async (request, response) => {
    const userDto : UserDto = request.body; // from body parser
    try {
        const user : User = await UserService.create(userDto);
        response.status(201).json({
            user,
            selfLink: `${urlGenerator.formUrlFromRoute(Routes.User)}/${user._id}`,
            createEventLink: `${urlGenerator.formUrlFromRoute(Routes.Event)}/createForUser/${user._id}`
        });
    } catch(error) {
        response.status(500).json(error);
    }
});