import express from 'express';
import EventService from '../services/eventService';
import paginationUtility from '../utilities/paginationUtility';
import { PaginatedResponse, SingleResponseModel } from '../http/httpTypes';
import { Routes } from './routesDefinition';
import urlGenerator from '../utilities/urlGenerator';
import { Event } from '../dataModel/event';

export const eventRouter = express.Router();

eventRouter.get('/', async (request, response) => {
    try {
        const page = Number(request.query.page) || 1;
        const limit = Number(request.query.limit) || 10;
        const paginationResults = await EventService.list(page, limit);
        const singleObjects : SingleResponseModel<Event>[] = paginationResults.results.map(result => (
            {
                result,
                selfLink: `${urlGenerator.formUrlFromRoute(Routes.Event)}/${result._id}`
            }
        ));
        const responseObject = paginationUtility.generatePaginationResult<Event>(Routes.Event, page, limit, singleObjects, paginationResults.count);
        response.status(200).json(responseObject);
    } catch (error) {
        response.status(500).json(error);
    }
});

eventRouter.get('/forUser', async (request, response) => {
    try {
        const id = request.query.userId;
        const page = Number(request.query.page) || 1;
        const limit = Number(request.query.limit) || 10;
        const paginationResults = await EventService.searchByUserId(id, page, limit);
        const singleObjects : SingleResponseModel<Event>[] = paginationResults.results.map(result => (
            {
                result,
                selfLink: `${urlGenerator.formUrlFromRoute(Routes.Event)}/${result._id}`
            }
        ));
        const responseObject = paginationUtility.generatePaginationResult(Routes.Event, page, limit, singleObjects, paginationResults.count);
        response.status(200).json(responseObject);
    } catch (error) {
        response.status(500).json(error);
    }
});

eventRouter.get('/today', async (request, response) => {
    try {
        const page = Number(request.query.page) || 1;
        const limit = Number(request.query.limit) || 10;
        const paginationResults = await EventService.searchByToday(page, limit);
        const singleObjects : SingleResponseModel<Event>[] = paginationResults.results.map(result => (
            {
                result,
                selfLink: `${urlGenerator.formUrlFromRoute(Routes.Event)}/${result._id}`
            }
        ));
        const responseObject = paginationUtility.generatePaginationResult(Routes.Event, page, limit, singleObjects, paginationResults.count);
        response.status(200).json(responseObject);
    } catch (error) {
        response.status(500).json(error);
    }
})

eventRouter.post('/createForUser/:id', async (request, response) => {
    try {
        const userId = request.params.id;
        const eventDto = request.body;
        const event = await EventService.create({type: eventDto.type, user: userId});
        response.status(201).json({
            event,
            selfLink: `${urlGenerator.formUrlFromRoute(Routes.Event)}/${event._id}`,
            getUserEventLink:`${urlGenerator.formUrlFromRoute(Routes.Event)}/forUser?userId=${userId}`
        });
    } catch (error) {
        response.status(500).json(error);
    }
})