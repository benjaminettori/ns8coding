import mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';

enum DbState {
    Error = 'error',
    Open = 'open'
}

export default class DatabaseConnection {
    dbServer = new MongoMemoryServer();

    getConnection = async () => {
        const connectionString = await this.dbServer.getUri();

        const mongooseOpts = {
            autoReconnect: true,
            reconnectTries: Number.MAX_VALUE,
            reconnectInterval: 1000
        };

        await mongoose.connect(connectionString, mongooseOpts);

        mongoose.connection.on(DbState.Error, (error : any) => {
            // tslint:disable-next-line:no-console
            console.log(error);
            if (error.message.code === 'ETIMEDOUT') {
                mongoose.connect(connectionString, mongooseOpts);
            }
        });

        mongoose.connection.once(DbState.Open, () => {
            // tslint:disable-next-line:no-console
            console.log(`MongoDB successfully connected to ${connectionString}`);
        });
    };

    closeConnection = async () => {
        await mongoose.connection.dropDatabase();
        await mongoose.connection.close();
        await this.dbServer.stop();
    }

    clearDatabase = async () => {
        const tables = mongoose.connection.collections;
        Object.entries(tables).forEach(async ([key, table]) => {
            await table.deleteMany({});
        });
    }
}