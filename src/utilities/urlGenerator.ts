export default {
    formUrlFromRoute: (routeName: string) : string => {
        return `${process.env.BASE_URL}:${process.env.SERVER_PORT}/${routeName}`
    },
    formNextPaginationUrl: (routeName: string, page: number, limit: number, count: number) : string => {
        const currentLimit = page * limit;
        return currentLimit > count ? null : `${process.env.BASE_URL}:${process.env.SERVER_PORT}/${routeName}?page=${page+1}&limit=${limit}`;
    },
    formPreviousPaginationUrl: (routeName: string, page: number, limit: number) : string => {
        return page <= 1 ? null : `${process.env.BASE_URL}:${process.env.SERVER_PORT}/${routeName}?page=${page-1}&limit=${limit}`;
    },
}