import urlGenerator from '../utilities/urlGenerator';
import { PaginatedResponse, SingleResponseModel } from '../http/httpTypes';

const paginationUtility = {
    generatePaginationLinks : (routeName: string, page: number, limit: number, count: number) : {nextLink: string, previousLink: string} => {
        return {
            nextLink: urlGenerator.formNextPaginationUrl(routeName, page, limit, count),
            previousLink: urlGenerator.formPreviousPaginationUrl(routeName, page, limit)
        }
    },

    generatePaginationResult: <T>(routeName: string, page: number, limit: number, data: SingleResponseModel<T>[], count: number) : PaginatedResponse<T> => {
        const responseObject : PaginatedResponse<T> = {
            data,
            count
        };

        const { nextLink, previousLink } = paginationUtility.generatePaginationLinks(routeName, page, limit, count);

        if(!!nextLink) {
            responseObject.nextLink = nextLink;
        }

        if (!!previousLink) {
            responseObject.previousLink = nextLink;
        }

        return responseObject;
    }
};

export default paginationUtility;