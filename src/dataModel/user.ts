import { Schema, Document, model } from 'mongoose';

export interface UserDto {
    email: string
    password: string
    phoneNumber?: string
}

export type User = UserDto & Document

const UserSchema : Schema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true,
        validate: {
            validator: (v: string) => {
                return /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/.test(v)
            },
            message: props => `${props.value} is not a valid email address!`
        }
    },
    password: {type: String, required: true},
    phoneNumber: {
        type: String,
        validate: {
            validator: (v: string) => {
              return /\d{3}-\d{3}-\d{4}/.test(v);
            },
            message: props => `${props.value} is not a valid phone number!`
        }
    },
    events: [{type: Schema.Types.ObjectId, ref: 'Event'}]
}, { timestamps: true });

export default model<User>('User', UserSchema);