import {Schema, Document, model} from 'mongoose';
import { User } from './user';

export interface EventDto {
    type: string
    user: User['_id']
}

export type Event = EventDto & Document

const EventSchema = new Schema({
    type: {type: String, required: true},
    user: {type: Schema.Types.ObjectId, ref: 'User'}
}, { timestamps: {createdAt: 'created'} }); // fill in creation timestamp

export default model<Event>('Event', EventSchema);