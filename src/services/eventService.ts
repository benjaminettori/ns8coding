import BaseService from './baseService';
import EventModel, { Event, EventDto } from '../dataModel/event';
import moment = require('moment');

export class EventService implements BaseService<Event, EventDto> {
    findById = async (id: string) : Promise<Event> => {
        return await EventModel.findById(id);
    };

    list = async (page: number, limit: number) : Promise<{ results: Event[]; count: number; }> => {
        const skip = Math.max(0, (page - 1)) * limit;
        const results = await EventModel.find().skip(skip).limit(limit);
        const count = await EventModel.count({});
        return {
            results,
            count
        };
    };

    searchByUserId = async (userId: string, page: number, limit: number) : Promise<{results: Event[], count: number;}> => {
        const skip = Math.max(0, (page - 1)) * limit;
        const results = await EventModel.find({user: userId}).skip(skip).limit(limit);
        const count = await EventModel.count({user: userId});
        return {
            results,
            count
        };
    };

    searchByToday = async (page: number, limit: number) : Promise<{results: Event[], count: number;}> => {
        const skip = Math.max(0, (page - 1)) * limit;
        const start = moment().startOf('day');
        const end = moment().endOf('day');
        const results = await EventModel.find({created: {'$gte': start, '$lte': end}}).skip(skip).limit(limit);
        const count = await EventModel.count({created: {'$gte': start, '$lte': end}});
        return {
            results,
            count
        };
    }

    create = async (dto: EventDto) : Promise<Event> => {
        return await EventModel.create(dto);
    };
}

const eventService = new EventService();
export default eventService;