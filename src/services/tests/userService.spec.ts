import DbConnection from '../../database/databaseConnection';
import userService from '../userService';

const dbConnection = new DbConnection();

beforeAll(async () => await dbConnection.getConnection());

afterEach(async () => dbConnection.clearDatabase());

afterAll(async () => await dbConnection.closeConnection())

describe('User Service Tests', () => {
    test('create user does not work if invalid email passed', async () => {
        await expect(userService.create({email: 'as', password: 'asfas'})).rejects.toThrow();
    });

    test('create user works with valid email and password only', async () => {
        const userDto = {email: 'ben.ettori@gmail.com', password: 'password'};
        const user = await userService.create(userDto);
        expect(user.email).toBe(userDto.email);
        expect(user.password).toBe(userDto.password);
    });

    test('create user will fail if phone number is not in correct format', async () => {
        const userDto = {email: 'ben.ettori@gmail.com', password: 'password', phoneNumber: '12321'};
        await expect(userService.create(userDto)).rejects.toThrow();
    });

    test('create user will works with valid phone number', async () => {
        const userDto = {email: 'ben.ettori@gmail.com', password: 'password', phoneNumber: '222-222-2222'};
        const user = await userService.create(userDto);
        expect(user.email).toBe(userDto.email);
        expect(user.password).toBe(userDto.password);
        expect(user.phoneNumber).toBe(userDto.phoneNumber);
    })
})