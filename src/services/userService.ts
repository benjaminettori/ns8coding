import UserModel, { User, UserDto } from '../dataModel/user';
import BaseService from './baseService';

export class UserService implements BaseService<User, UserDto> {
    create = async (dto: UserDto) : Promise<User> => {
        return await UserModel.create(dto);
    }

    findById = async (id: string) : Promise<User> => {
        return await UserModel.findById(id).lean();
    }

    findByEmail = async (email: string) : Promise<User> => {
        return await UserModel.findOne({email}).lean();
    }

    list = async (page: number, limit: number) : Promise<{results: User[], count: number}> => {
        const skip = Math.max(0, (page - 1)) * limit;
        const results : User[] = await UserModel.find().skip(skip).limit(limit).lean();
        const count = await UserModel.count({});
        return {
            results,
            count
        }
    }
}

const userService = new UserService();
export default userService;