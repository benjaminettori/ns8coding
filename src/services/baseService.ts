import { Document } from 'mongoose';

export default interface BaseService<T extends Document, U> {
    create: (dto: U) => Promise<T>
    findById: (id: string) => Promise<T>
    list: (page: number, limit: number) => Promise<{results: T[], count: number}>
}