import express from 'express';
import dotenv from 'dotenv';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import { userRouter } from './routes/userRoute';
import { eventRouter } from './routes/eventRoute';
import DbConnection from './database/databaseConnection';
import { Routes } from './routes/routesDefinition';

dotenv.config();
const app = express();
const port = process.env.SERVER_PORT || 8080;

// Middleware setup
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// CORS
app.use((request, response, next) => {
    response.header('Access-Control-Allow-Origin', '*');
    response.header('Acecss-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');

    if(request.method === 'OPTIONS') {
        response.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return response.status(200).json({});
    }
    next();
});

app.use(`/${Routes.User}`, userRouter);
app.use(`/${Routes.Event}`, eventRouter);

// define a route handler for the default home page
app.get('/', (req, res) => {
    res.send('Hello world!');
});

// start the Express server
// Get db connection
const dbConnection = new DbConnection();
dbConnection.getConnection().then(() => {
    app.listen(port);
});