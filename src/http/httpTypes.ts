export interface PaginatedResponse<T> {
    data: SingleResponseModel<T>[]
    count: number
    nextLink?: string
    previousLink?: string
}

export interface SingleResponseModel<T> {
    result: T
    selfLink: string
}