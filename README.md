***Please copy the provided .example_env file to a .env file in the project directory.***

I did not commit the .env file because it is bad practice.

## Install dependencies

```
yarn
```

Run server

```
yarn dev
```

Run unit tests

```
yarn test
```

## Description of endpoints

To get users as a paginated result

```
GET /user?page=...&limit=... (defaults to 1 and 10 respectively)
```

To create a user

```
POST /user
```

with body

```
{
    "email": "joe@umail.com",
	"password": "password",
	"phoneNumber": "333-333-3333"
}
```

To find user by id

```
GET /user/:userId
```

To find user by email

```
GET /user/find/:email
```

To create event for user

```
POST /event/createForUser/:userId
```

with body

```
{
    "type": <event_type>
}
```

To find events for user (same page and limit defaults)

```
GET /event/forUser?userId=...&page=...&limit=...
```

To find events for today (same page and limit defaults)

```
GET /event/today?page=...&limit=...
```

List of things I did not have time to do:
- Auth implementation
- enabling caching
- Implement configuration layer
- Finish Unit Tests
- Localize message strings
- Implement PUT, PATCH verbs for the endpoints
- Refactor some of the pagination results code